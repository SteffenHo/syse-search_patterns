package syse_search_patterns.syse_search_patterns;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class AutocompleteWithoutDropdown  {
	
    public static void main( String[] args ) {
    	JFrame meinJFrame = new JFrame();
    	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

    	
        meinJFrame.setTitle("JTextFieldBeispiel");
        meinJFrame.setSize(300, 150);
        JPanel panel = new JPanel();
 
        JLabel label = new JLabel("Suche");
        panel.add(label);
 
        // Textfeld wird erstellt
        // Text und Spaltenanzahl werden dabei direkt gesetzt
        final JTextField mainTextField = new JTextField("", 20);
        
        
        // Our words to complete
		ArrayList<String>   keywords = new ArrayList<String>(5);
		keywords.add("example");
		keywords.add("extra");
		keywords.add("autocomplete");
		keywords.add("stackabuse");
		keywords.add("java");
		
		final AutocompleteWithoutDHelper autoComplete = new AutocompleteWithoutDHelper(mainTextField, keywords);
		
		autoComplete.addKeyword("peter");
		mainTextField.getDocument().addDocumentListener(autoComplete);
        
		autoComplete.addKeyword("tinkerbell");
		
     // Without this, cursor always leaves text field
        mainTextField.setFocusTraversalKeysEnabled(false);

        
        // Textfeld wird unserem Panel hinzugefügt
        panel.add(mainTextField);
 
        JButton buttonAdd = new JButton("Add");
        buttonAdd.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				autoComplete.addKeyword(mainTextField.getText());
				mainTextField.setText("");
				
			}
		});
        panel.add(buttonAdd);
 
        meinJFrame.add(panel);
        
    	meinJFrame.setLocation(dim.width/2-meinJFrame.getSize().width/2, dim.height/2-meinJFrame.getSize().height/2);
        meinJFrame.setVisible(true);
    }
    
    
    
}


