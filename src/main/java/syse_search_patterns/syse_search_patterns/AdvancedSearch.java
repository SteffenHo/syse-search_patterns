package syse_search_patterns.syse_search_patterns;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AdvancedSearch {
	 public static void main( String[] args ) {
	    	JFrame meinJFrame = new JFrame();
	    	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

	    	final ArrayList<AdvancedSearchObject> generalObjects = new ArrayList<AdvancedSearchObject>();
	    	generalObjects.add(new AdvancedSearchObject("birke", "baum", Arrays.asList("natur", "pflanze", "allergie")));
	    	generalObjects.add(new AdvancedSearchObject("tanne", "baum", Arrays.asList("natur", "pflanze", "allergie")));
	    	generalObjects.add(new AdvancedSearchObject("apfel", "rot", Arrays.asList("natur", "obst", "baum")));
	    	generalObjects.add(new AdvancedSearchObject("birne", "grün", Arrays.asList("natur", "obst", "baum")));
	    	
	    	final ArrayList<AdvancedSearchObject> fruitObjects = new ArrayList<AdvancedSearchObject>();
	    	fruitObjects.add(new AdvancedSearchObject("banane", "gelb", Arrays.asList("natur", "obst", "baum")));
	    	fruitObjects.add(new AdvancedSearchObject("annanas", "braun", Arrays.asList("natur", "obst", "strauch")));
	    	fruitObjects.add(new AdvancedSearchObject("kiwi", "grün", Arrays.asList("natur", "obst", "strauch")));
	    	fruitObjects.add(new AdvancedSearchObject("erdbeere", "rot", Arrays.asList("natur", "obst", "strauch")));
	    	
	        meinJFrame.setTitle("Search Pattern Beispiel");
	        JPanel panel = new JPanel();
	        panel.setLayout(new BoxLayout(panel, 1));
	 
	        JLabel label = new JLabel("Search");
	        panel.add(label);
	 
	        // Textfeld wird erstellt
	        // Text und Spaltenanzahl werden dabei direkt gesetzt
	        final JTextField mainTextField = new JTextField("", 20);
	        // Textfeld wird unserem Panel hinzugefügt
	        panel.add(mainTextField);
	 
	        //Area für Ergebnisse
	        final JTextArea textfeld = new JTextArea(5, 20);
	        
	        //Checkboxen Suchoption
	        JPanel checkBoxPanel = new JPanel();
	        final JCheckBox exchangingCard1 = new JCheckBox("Include description");
	        checkBoxPanel.add(exchangingCard1);
	        final JCheckBox exchangingCard2 = new JCheckBox("Include tags");
	        checkBoxPanel.add(exchangingCard2);
	        
	        //Radio Buttons
	        JPanel radioButtonPanel = new JPanel();
	        final JRadioButton button1 = new JRadioButton("LIKE");
	        button1.setSelected(true);
	        final JRadioButton button2 = new JRadioButton("CONTAINS");
	        ButtonGroup group = new ButtonGroup();
	        group.add(button1);
	        group.add(button2);
	        radioButtonPanel.add(button1);
	        radioButtonPanel.add(button2);
	        
	        //Checkboxen Quellen
	        JPanel checkBoxPanel2 = new JPanel();
	        final JCheckBox exchangingCard3 = new JCheckBox("Include Source General");
	        exchangingCard3.setSelected(true);
	        checkBoxPanel2.add(exchangingCard3);
	        final JCheckBox exchangingCard4 = new JCheckBox("Include Source Fruits");
	        checkBoxPanel2.add(exchangingCard4);
	        
	        //Such Button
	        JButton buttonSearch = new JButton("Search");
	        buttonSearch.addActionListener(new ActionListener() {	        	
				public void actionPerformed(ActionEvent e) {
					String search = mainTextField.getText();
					textfeld.setText("");
					
					final ArrayList<AdvancedSearchObject> objects = new ArrayList<AdvancedSearchObject>();
					if (exchangingCard3.isSelected()) {
						objects.addAll(generalObjects);
					}
					if (exchangingCard4.isSelected()) {
						objects.addAll(fruitObjects);
					}
					
					for (AdvancedSearchObject a : objects) {						
						//LIKE Suchoption
						if (button1.isSelected()) {
							if(a.getTitle().equals(search)) {
							textfeld.append(a.getTitle() + "\n");
							continue;
							}
							if(a.getDescription().equals(search) && exchangingCard1.isSelected()) {
								textfeld.append(a.getTitle() + "\n");
								continue;
							}
							
							for (String s : a.tags) {
								if(s.equals(search) && exchangingCard2.isSelected()) {
								textfeld.append(a.getTitle() + "\n");
								continue;
								}
							}
						}
						
						//Contains Suchoption
						else if (button2.isSelected()) {
							if(a.getTitle().contains(search)) {
							textfeld.append(a.getTitle() + "\n");
							continue;
							}
							if(a.getDescription().contains(search) && exchangingCard1.isSelected()) {
								textfeld.append(a.getTitle() + "\n");
								continue;
							}
							
							for (String s : a.tags) {
								if(s.contains(search) && exchangingCard2.isSelected()) {
								textfeld.append(a.getTitle() + "\n");
								continue;
								}
							}
						}
						
					}
					
				}
			});
	        
	        //Komponenten zu Panel hinzufügen
	        panel.add(buttonSearch);	        
	        panel.add(checkBoxPanel);	
	        panel.add(checkBoxPanel2);	
	        panel.add(radioButtonPanel);	        
	        panel.add(textfeld);
	        
	        
	        meinJFrame.add(panel);       
	        meinJFrame.setSize(500, 300);
	    	meinJFrame.setLocation(dim.width/2-meinJFrame.getSize().width/2, dim.height/2-meinJFrame.getSize().height/2);
	        meinJFrame.setVisible(true);
	    }
}
