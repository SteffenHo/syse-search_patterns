package syse_search_patterns.syse_search_patterns;

import java.util.List;

public class AdvancedSearchObject {

	String title;
	String description;
	List<String> tags;
	
	public AdvancedSearchObject(String title, String description, List<String> tags) {
		this.title = title;
		this.description = description;
		this.tags = tags;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	
}


